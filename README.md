Ce script permets de passer un site svn vers git.

# REQUIS
- https://git.spip.net/spip-contrib-outils/checkout à la racine de votre hébergement
- svn_git doit se trouver au même niveau que checkout

# Variable
 - repsvn => mettre le dossier ou vous avez vos scripts
 - core => indiquez un dossier par ligne