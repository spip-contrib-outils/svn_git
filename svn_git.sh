#!/bin/bash
#
# Le script fait le travail suivant :
# - Mettre spip en git dans le dossier git_$site
# - Ajout des plugins present en prod dans le site git_$site
# - Copie depuis le site de prod de : config IMG lib htaccess
# - Renomme le site de prod avec svn_
# - Renomme le site git_ en prod

if [ "checkout" ]; then
php checkout/checkout.php git -bmaster https://git.spip.net/spip-contrib-outils/checkout.git checkout;
else
git clone https://git.spip.net/spip-contrib-outils/checkout.git checkout
fi

# Répertoire des sites
repsvn=""

# Liste des sites à mettre à jour
core=""

for site in $core; do
    echo $site;
    # Mettre spip en git
    php checkout/checkout.php spip -bspip-3.2 $repsvn/git_$site/
    # Plugins à passer sur le git
    for file in $repsvn/$site/plugins/*
    do
    	php checkout/checkout.php git -bmaster https://git.spip.net/spip-contrib-extensions/$(basename $file).git $repsvn/git_$site/plugins/$(basename $file);
    done
    # plugins-dist à rajouter
    php checkout/checkout.php git -bmaster https://git.spip.net/spip-contrib-extensions/cache_cool.git $repsvn/git_$site/plugins-dist/cache_cool/
    php checkout/checkout.php git -bmaster https://git.spip.net/spip-contrib-extensions/nospam.git $repsvn/git_$site/plugins-dist/nospam/
    php checkout/checkout.php git -bmaster https://git.spip.net/spip-contrib-extensions/memoization.git $repsvn/git_$site/plugins-dist/memoization/
    # copie de la config du site prod vers git
    rm -rf $repsvn/git_$site/config/*
    cp -avr $repsvn/$site/config/ $repsvn/git_$site/
    rm -rf $repsvn/git_$site/IMG/*
    cp -avr $repsvn/$site/IMG/ $repsvn/git_$site/
    cp -avr $repsvn/$site/lib/ $repsvn/git_$site/
    cp -avr $repsvn/$site/.htaccess $repsvn/git_$site/.htaccess
    # mise en prod
    mv $repsvn/$site/ $repsvn/svn_$site/
    mv $repsvn/git_$site/ $repsvn/$site/
done